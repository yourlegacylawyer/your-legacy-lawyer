Bonnie Bowles | Estate Planning | Probate | Wills | Trusts 
By virtual appointment.

My sole focus is to help you and your family avoid collapse after the death or disability of a loved one. By leveraging online technology to meet virtually, together we craft your vision into an estate plan that helps you throughout your lifetime and your family when they need it.

I serve families in Colorado, Texas, and New Mexico.

Visit https://yourlegacylawyer.com/ to request one of my free resources to get started.